import React from 'react'
import { connect } from 'react-redux'

import stylesPage from '../styles/UserPage.module.scss'
import stylesMain from '../styles/App.module.scss'
import { Paper } from '@material-ui/core';
import { clearUserData, fetchUserById, fetchUserByShort } from '../reducers/user';

class UserPage extends React.Component {
    localClasses = { ...stylesMain, ...stylesPage }

    componentWillMount() {
        if (this.props.match.params.userId)
            this.props.requestUserById(this.props.match.params.userId)
        else if (this.props.match.params.userShort)
            this.props.requestUserByShort(this.props.match.params.userShort)
    }

    componentDidUpdate() {
        if (this.props.errorRecieve)
            this.props.history.push("/")
        document.title = this.props.userData ? this.props.userData.login : "User";
    }

    componentWillUnmount() {
        this.props.clearUserData()
    }
    
    render() {
        return this.props.userData ? (
            <div>
                <Paper className={`
                    ${this.localClasses.page}
                    ${this.localClasses.user_page}
                `}>
                    <div>{this.props.userData.login}</div>
                    <div>{this.props.userData.status}</div>
                </Paper>
            </div>
        ) : ""
    }
}

const mapStateToProps = (state) => {
    return {
        userData: state.user.userData
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        requestUserById: (userId) => dispatch(fetchUserById(userId)),
        requestUserByShort: (userShort) => dispatch(fetchUserByShort(userShort)),
        clearUserData: () => dispatch(clearUserData()),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(UserPage)