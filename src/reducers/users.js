import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'

const USERS_REQUEST = "/api/get-users"

export const requestUsersAsync = createAsyncThunk(
    'users/requestUsers',
    async (thunkAPI) => {
        const response = await fetch("https://itclass.pythonanywhere.com" + USERS_REQUEST)
        return response.json()
    }
)

const slice = createSlice({
    name: 'users',
    initialState: {
        users: null,
        curPage: 1,
        countUsersOnPage: 5,
    },
    reducers: {
        changePage(state, action) {
            state.curPage = action.payload
        },
        changeSize(state, action) {
            state.curPage = 1
            state.countUsersOnPage = action.payload
        }
    },
    extraReducers: {
        [requestUsersAsync.fulfilled]: (state, action) => {
            let buf = {"admin": 0, "user": 1}
            action.payload.sort((a, b) => (buf[a.role] - buf[b.role]))
            state.users = action.payload
            state.curPage = 1
        }
    }
})

export const { changePage, changeSize } = slice.actions;
export default slice.reducer;