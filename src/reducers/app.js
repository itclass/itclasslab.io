import { createSlice } from "@reduxjs/toolkit"

const slice = createSlice({
    name: 'app',
    initialState: {
        user: {
            isAuth: false,
            data: null,
        },
        isMobile: false,
        isTablet: false,
    },
    reducers: {
        setMobile(state, action) {
            state.isMobile = action.payload
        },
        setTablet(state, action) {
            state.isTablet = action.payload
        }
    }
});

const { actions, reducer } = slice
export const { setMobile, setTablet } = actions
export default reducer