import { createSlice } from '@reduxjs/toolkit'

const TIMETABLE_REQUEST = "/api/timetable"

const processTables = tables => {
    const weekdays = tables.data.weekdays
    const weekend = tables.data.weekend
    const timetable = []
    const maxCount = Math.max(...tables.data.timetable.map(i => i.lessons.length))

    for (let i = 0; i < maxCount; i++) {
        const row = []
        for (let j = 0; j < 6; j++) {
            row.push(tables.data.timetable[j].lessons[i])
        }
        timetable.push(row)
    }

    return {
        timetable,
        weekdays,
        weekend
    }
}

const requestTablesFetch = (dispatch) => {
    fetch("https://itclass.pythonanywhere.com" + TIMETABLE_REQUEST)
        .then(res => res.json())
        .then(response => dispatch(response))
}

const slice = createSlice({
    name: 'timetable',
    initialState: {
        timetable: null,
        weekdays: null,
        weekend: null
    },
    reducers: {
        recieveTables(state, action) {
            let tables = processTables(action.payload);
            state.timetable = tables.timetable;
            state.weekdays = tables.weekdays;
            state.weekend = tables.weekend;
        },
        requestTables(state, action) {
            requestTablesFetch(action.payload)
        }
    }
})

const { actions, reducer } = slice;
export const { recieveTables, requestTables } = actions;
export default reducer;