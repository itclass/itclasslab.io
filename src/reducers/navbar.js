import HomeIcon from '@material-ui/icons/Home'
import PeopleIcon from '@material-ui/icons/People'
import GradeIcon from '@material-ui/icons/Grade'
import ViewWeekIcon from '@material-ui/icons/ViewWeek'
import ChevronRightIcon from '@material-ui/icons/ChevronRight'
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft'
import LockIcon from '@material-ui/icons/Lock'
import { createSlice } from '@reduxjs/toolkit'

const slice = createSlice({
    name: 'navbar',
    initialState: {
        openLeftMenu: false,
        openRightMenu: false,
        linksMenuLeft: [
            ["Главная", HomeIcon, "/"],
            ["Пользователи", PeopleIcon, "/users"],
            ["Имиджборд", GradeIcon, "/threads"],
            ["Расписание", ViewWeekIcon, "/timetable"],
        ],
        linksMenuRightUnauth: [
            ["Вход", ChevronRightIcon, "/signin"],
            ["Регистрация", LockIcon, "/signup"],
        ],
        linksMenuRightAuth: [
            ["Профиль", HomeIcon, "/"],
            ["Редактировать", PeopleIcon, "/users"],
            ["Оформление", GradeIcon, "/trends"],
            ["Выйти", ChevronLeftIcon, "/logout"]
        ]
    },
    reducers: {
        menuLeftChangeActionCreator(state, action) {
            state.openLeftMenu = action.payload;
        },
        menuRightChangeActionCreator(state, action) {
            state.openRightMenu = action.payload;
        }
    }
})

const { actions, reducer } = slice;
export const { menuLeftChangeActionCreator, menuRightChangeActionCreator } = actions;
export default reducer;