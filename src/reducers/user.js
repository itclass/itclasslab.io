import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'

const USER_REQUEST = "/api/get-user"

export const fetchUserById = createAsyncThunk(
    'user/fetchByIdStatus',
    async (userId, thunkAPI) => {
        const response = await fetch("https://itclass.pythonanywhere.com" + USER_REQUEST + "?id=" + userId)
        return response.json()
    }
)

export const fetchUserByShort = createAsyncThunk(
    'user/fetchByShortStatus',
    async (userShort, thunkAPI) => {
        const response = await fetch("https://itclass.pythonanywhere.com" + USER_REQUEST + "?short=" + userShort)
        return response.json()
    }
)

const slice = createSlice({
    name: 'user',
    initialState: {
        userData: null
    },
    reducers: {
        clearUserData(state) {
            state.userData = null
        }
    },
    extraReducers: {
        [fetchUserById.fulfilled]: (state, action) => {
            state.userData = action.payload
        },
        [fetchUserByShort.fulfilled]: (state, action) => {
            state.userData = action.payload
        }
    }
})

export const { clearUserData } = slice.actions;
export default slice.reducer;