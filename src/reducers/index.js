import { combineReducers, configureStore } from '@reduxjs/toolkit'
import navbarReducer from './navbar'
import appReducer from './app'
import timetableReducer from './timetable'
import usersReducer from './users'
import userReducer from './user';

const allReducers = combineReducers({
    navbar: navbarReducer,
    app: appReducer,
    timetable: timetableReducer,
    users: usersReducer,
    user: userReducer,
})

export const store = configureStore({
    reducer: allReducers
});
